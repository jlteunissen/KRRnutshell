import numpy as np
from sklearn.base import BaseEstimator

class KRR(BaseEstimator):
    def __init__(self, ktype='gaussian', labda=1.e-5, sigma=1.e-5):
        self.ktype =ktype
        self.labda =labda
        self.sigma =sigma

    def fit(self, X, y):
        self.X=X
        self.y=y
        self.kernel=self.get_kernel(X,X)
        self.solver() #sets the alpha vector
        return self

    def predict(self, Xtest):
        L = self.get_kernel(self.X, Xtest)
        y = np.dot(L.T, self.alpha)
        return y

    def get_kernel(self, X1, X2):
        l1 = len(X1) # n coulomb matrices in X1
        l2 = len(X2) # n coulomb matrices in X2
        K = np.zeros([l1,l2])
        print "Kshape:", K.shape
        # calculate denominator:
        if self.ktype == 'gaussian':
            factor = 1. / ( 2. * (self.sigma**2) )
        elif self.ktype == 'laplacian':
            factor = 1. / self.sigma
        for i in range(l1):
            for j in range(l2): #for i in range(0) gives []
                d = distance(X1[i], X2[j])
                K[i][j] = np.exp( - d * factor )
        return K

    def solver(self):
        l = len(self.kernel)
        I = np.identity(l)
        Ka = self.kernel + self.labda * I

        # do cholesky solv:
        from scipy import linalg
        cho = linalg.cho_factor(Ka)
        alpha = linalg.cho_solve(cho, self.y)

        print "alphashape:", alpha.shape
        self.alpha=alpha
        return

def distance(C1, C2, kerneltype='gaussian'):
    '''calculate euclidian distance between two coulomb matrices'''
    assert C1.shape==C2.shape, 'coulomb matrices not the same size'
    # make a difference array D
    D = C1.copy()
    D -= C2
    if kerneltype == "gaussian":
        # sum of quadratic differences
        total = np.sum( D**2 )
    elif kerneltype == "laplacian":
        total = np.sum( np.abs(D) )
    return total

