""" quantum chemical machine learning tutorial """

# import statements:
import numpy as np
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import GridSearchCV
from utils import visualize, notHtable, yyplot, yystats
from krr import KRR

def read():
    masses = { 'X': 0, 'C': 12.011, 'F': 18.9984032, 
               'H': 1.00794, 'N': 14.00674, 'O': 15.9994,
               'P': 30.973762, 'S': 32.066  }
    XYZ=[]
    E=[]
    maxnatoms=1000
    natoms=0
    with open('dsgdb7ae2.xyz') as f:
        for line in f:
            natoms+=1
            if maxnatoms<=natoms:break
            # 1. read natoms
            natoms=int(line.strip())
            # 2. go to next line and read the atomization energy
            line=next(f)
            E.append(float(line.strip().split()[1]))
            # 3. read the xyz coordinates
            xyz=[]
            for _ in xrange(natoms):
                # choosing to read the FF xyz coordinates
                splitted =next(f).strip().split()
                atom = [ splitted[0], np.array(map(float, splitted[1:4])), masses[splitted[0]]]
                xyz.append(atom)
            XYZ.append(xyz)
    return XYZ, E

def coulomb(xyz, largest_mol_size=None):
    """
    This function generates a coulomb matrix for the given molecule
    if largest_mol size is provided matrix will have dimension lm x lm.
    Padding is provided for the bottom and right _|
    """
    def symsort(mat):
        indexlist = np.argsort(np.linalg.norm(mat,axis=1))[::-1]
        return mat[indexlist][:,indexlist]

    numberAtoms = len(xyz)
    if largest_mol_size == None or largest_mol_size == 0: largest_mol_size = numberAtoms
    C = np.zeros([largest_mol_size, largest_mol_size])

    xyzmatrix = [atom[1] for atom in xyz]
    chargearray = [atom[2] for atom in xyz]

    for i in range(numberAtoms):
        for j in range(i+1):
            if i == j:
                C[i][j] = 0.5 * chargearray[i] ** 2.4  # Diagonal term described by Potential energy of isolated atom
            else:
                dist = np.linalg.norm(np.array(xyzmatrix[i]) - np.array(xyzmatrix[j]))
                C[i][j] = chargearray[i] * chargearray[j] / dist  # Pair-wise repulsion
                C[j][i] = C[i][j] # this is still necessary to get the correct norm for sorting the matrix
    
    # sort the matrix based on the norm:
    C = symsort(C)
    # return only lower triangular form
    return C[ np.tril_indices(largest_mol_size) ]

def single_krr(X_train, y_train, X_holdout, y_holdout, labda, sigma):
    # here I should convert labda and sigma to alpha and gamma values
    if True: # use my own KernelRidge implementation
        krr = KRR(ktype='gaussian',
              labda = labda,
              sigma = sigma,
              )
    else: # use sklearn KernelRidge
        alpha=labda
        gamma=1. / (2*sigma**2)
        krr = KernelRidge(kernel='rbf',
              alpha = alpha,
              gamma = gamma,
              )
    print "Fitting..."
    krr.fit(X_train, y_train)
    print "\tLearned model: ", krr
    y_holdout_pred = krr.predict(X_holdout)
    y_train_pred = krr.predict(X_train)
    print "y_holdout_pred:", y_holdout_pred
    print "stats train:"
    yystats(y_train_pred, y_train)
    print "stats holdout:"
    yystats(y_holdout_pred, y_holdout)
    yyplot(y_holdout_pred, y_holdout, y_train_pred, y_train)
    return

def gridsearch():
    # 1. set hyperparamter search
    hparam_grid = {'alpha': np.logspace(-10,0,5),
                   'gamma': np.logspace(-10,0,5),
                   'kernel': ['rbf','laplacian'] }
    clf_gn = GridSearchCV( krr_rbf,
                           cv= 3,
                           param_grid = hparam_grid )
    
    return

def split(X, y, size=100, include_smalls=None):
    if include_smalls is None:
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split( X, y, 
                train_size=size, random_state = 1234 )
    elif include_smalls:
        # take every X with 4 or less hydrogen atoms.(k in total)
        # from the others take every (7102-k/1000-k)th atom after sorting bases on n nonH atoms
        raise NotImplementedError('too lazy')
    else:
        # hold out set should not contain the small molecules
        raise NotImplementedError('too lazy')
    return X_train, X_test, y_train, y_test

def main():
    # 1. read input
    XYZ, E = read()
    maxnatoms=max(map(len,XYZ))
    print "maxnatoms:",maxnatoms
    if True:notHtable(XYZ)

    if True:# visualize a molecule:
        visualize(XYZ[2])

    # 2. calculate descriptor
    Cs = map(lambda x:coulomb(x, largest_mol_size=maxnatoms), XYZ)
    X  = np.array(map(lambda x:x.flatten(), Cs))
    print filter(bool,X[2]) # print only the non-zero entries in X

    # 3. split database in training and prediction set
    y=E
    X_train, X_pred, y_train, y_pred = split( X, y, size=1000, include_smalls=None )

    # 4. split_off a hold-out set
    X_holdout, X_train, y_holdout, y_train = split( X_train, y_train, size=100, include_smalls=None )

    # 5. train on the training set and report error on the holdout set
    single_krr(X_train, y_train, X_holdout, y_holdout, labda=1e-5, sigma=100)

    # 6. GridSearch. etc.  
    pass

    return 


if __name__=="__main__":
    main()
