def visualize(coords):
    ''' this makes a Gaussian input file. you can open the xyz.com file with:
    $> module load g16.A.03
    $> gview xyz.com
    '''
    xyz=[ ' '.join([str(item[0])]+map(str,item[1])) for item in coords ]
    xyz='\n'.join(xyz)
    print xyz
    with open('xyz.com','w') as f:
        f.write('# pm6\n\ntitle\n\n0 1\n')
        f.write(xyz)
        f.write('\n\n')
    return

def notHtable(xyzs):
    from collections import Counter
    getatoms = lambda x:[item[0] for item in x]
    notH = lambda x: not x=='H'
    nnotH= lambda x: len(filter(notH, x))
    atoms= map(getatoms, xyzs)
    notHs= map(nnotH, atoms)
    print "Number of non-hydrogen atoms:", Counter(notHs)
    return

def yystats(y1, y2):
    import numpy as np
    MAE = sum(abs(y1-y2))/len(y1)
    RMSE= np.sqrt(np.mean( (y1-y2)**2 ))
    print "MAE: {} | RMSE: {}".format(MAE, RMSE)
    return

def yyplot(y_real, y_pred, y_real2=None, y_pred2=None):
    import matplotlib.pyplot as plt
    plt.scatter(y_real, y_pred)
    if (not y_real2 is None) and (not y_pred2 is None):
        plt.scatter(y_real2, y_pred2)
    plt.show()
    return


